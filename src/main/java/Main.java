/**
 * Created by jan_w on 01.10.2017.
 */
public class Main {

    public static void main(String[] args) {

        Hero janusz = new Hero.Builder().setM_name("Janusz").setM_hp(44).setM_isAlive(true).setM_level(3).create();
        Hero grazyna = new Hero.Builder().setM_name("Grażyna").setM_strength(100).setM_level(2).create();

        System.out.println(janusz);
    }




}
